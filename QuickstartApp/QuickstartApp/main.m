//
//  main.m
//  QuickstartApp
//
//  Created by Thuong Nguyen on 3/13/17.
//  Copyright © 2017 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
