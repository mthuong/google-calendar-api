//
//  ViewController.m
//  QuickstartApp
//
//  Created by Thuong Nguyen on 3/13/17.
//  Copyright © 2017 com.example. All rights reserved.
//

#import "ViewController.h"

//#import "GTMOAuth2ViewControllerTouch.h"
#import "GTLCalendar.h"

#import <Google/SignIn.h>

static NSString *const kKeychainItemName = @"Google Calendar API";

@interface ViewController () <GIDSignInUIDelegate>

@property (nonatomic, strong) GTLServiceCalendar *service;
@property(strong) GTLCalendarCalendarList *calendarList;
@property(strong) GTLServiceTicket *calendarListTicket;
@property(strong) NSError *calendarListFetchError;

@property(strong) GTLServiceTicket *editEventTicket;

@property(weak, nonatomic) IBOutlet GIDSignInButton *signInButton;


@end

@implementation ViewController

// When the view loads, create necessary subviews, and initialize the Google Calendar API service.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
}

- (IBAction)btnFetchCalendarListClicked:(id)sender {
    [self updateAuthService];
    [self fetchCalendarList];
}

- (IBAction)btnAddEventClicked:(id)sender {
    [self updateAuthService];
    [self addAnEvent];
}

#pragma mark - === GIDSignInUIDelegate ===

- (IBAction)didTapSignOut:(id)sender {
    [[GIDSignIn sharedInstance] signOut];
}

// Implement these methods only if the GIDSignInUIDelegate is not a subclass of
// UIViewController.

// Stop the UIActivityIndicatorView animation that was started when the user
// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - === Authenticate Service ===

/**
 Call this method after user sign in google
 */
- (GTLServiceCalendar *)updateAuthService{
    if (self.service == nil) {
        // Initialize the Google Calendar API service & load existing credentials from the keychain if available.
        self.service = [[GTLServiceCalendar alloc] init];
    }
    self.service.authorizer = [[GIDSignIn sharedInstance] currentUser].authentication.fetcherAuthorizer;
    
    return self.service;
    
}

#pragma mark Fetch Calendar List

- (void)fetchCalendarList {
    self.calendarList = nil;
    self.calendarListFetchError = nil;
    
    GTLServiceCalendar *service = self.service;
    
    GTLQueryCalendar *query = [GTLQueryCalendar queryForCalendarListList];
    
    BOOL shouldFetchedOwned = true;
    if (shouldFetchedOwned) {
        query.minAccessRole = kGTLCalendarMinAccessRoleOwner;
    }
    
    self.calendarListTicket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, id calendarList, NSError *error) {
        // Callback
        self.calendarList = calendarList;
        self.calendarListFetchError = error;
        self.calendarListTicket = nil;
    }];
}

- (GTLCalendarCalendarListEntry *)selectedCalendarListEntry {
    if (self.calendarList.items.count > 0) {
        return self.calendarList.items.firstObject;
    }
    
    return nil;
}

#pragma mark Add, Edit, and Delete an Event

- (void)addAnEvent {
    // Make a new event, and show it to the user to edit
    GTLCalendarEvent *newEvent = [GTLCalendarEvent object];
    newEvent.summary = @"Sample Added Event";
    newEvent.descriptionProperty = @"Description of sample added event";
    
    // We'll set the start time to now, and the end time to an hour from now,
    // with a reminder 10 minutes before
    NSDate *anHourFromNow = [NSDate dateWithTimeIntervalSinceNow:(60 * 60)];
    
    // Include an offset minutes that tells Google Calendar that these dates
    // are for the local time zone.
    NSInteger offsetMinutes = [NSTimeZone localTimeZone].secondsFromGMT / 60;
    
    GTLDateTime *startDateTime = [GTLDateTime dateTimeWithDate:[NSDate dateWithTimeIntervalSinceNow:offsetMinutes] timeZone:[NSTimeZone localTimeZone]];
    GTLDateTime *endDateTime = [GTLDateTime dateTimeWithDate:[anHourFromNow dateByAddingTimeInterval:offsetMinutes] timeZone:[NSTimeZone localTimeZone]];
    
    newEvent.start = [GTLCalendarEventDateTime object];
    newEvent.start.dateTime = startDateTime;
    
    newEvent.end = [GTLCalendarEventDateTime object];
    newEvent.end.dateTime = endDateTime;
    
    GTLCalendarEventReminder *reminder = [GTLCalendarEventReminder object];
    reminder.minutes = @10;
    reminder.method = @"email";
    
    newEvent.reminders = [GTLCalendarEventReminders object];
    newEvent.reminders.overrides = @[ reminder ];
    newEvent.reminders.useDefault = @NO;
    
    [self addEvent:newEvent];
    
}

- (void)addEvent:(GTLCalendarEvent *)event {
    GTLServiceCalendar *service = self.service;
    
    GTLCalendarCalendarListEntry *selectedCalendar = [self selectedCalendarListEntry];
    NSString *calendarID = selectedCalendar.identifier;
    
    GTLQueryCalendar *query = [GTLQueryCalendar queryForEventsInsertWithObject:event calendarId:calendarID];
    
    self.editEventTicket = [service executeQuery:query completionHandler:^(GTLServiceTicket *ticket, GTLCalendarEvent *event, NSError *error) {
        // Callback
        self.editEventTicket = nil;
        if (error == nil) {
            NSLog(@"Event Added: %@", event.summary);
            
//            [self fetchSelectedCalendar];
        } else {
            NSLog(@"Add failed: %@", error);
        }
    }];
}



@end


