//
//  AppDelegate.h
//  QuickstartApp
//
//  Created by Thuong Nguyen on 3/13/17.
//  Copyright © 2017 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

